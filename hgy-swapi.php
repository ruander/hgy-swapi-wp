<?php
/**
 * @package Hgy_Swapi
 * @version 0.1
 */
/*
Plugin Name: George's Star Wars API sample file
Plugin URI: https://wordpress.org/plugins/swapi/
Description: Funny Star Wars character generator widget sample with shortcode functionality
Author: George Horvath
Version: 0.1
Author URI: https://iworkshop.hu/

George's Star Wars API sample file is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
any later version.
 
George's Star Wars API sample file is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.
 
You should have received a copy of the GNU General Public License
along with George's Star Wars API sample file. If not, see {License URI}.
*/

function hgy_test()
{
    $string = 'hello, fut az első <b>plugin</b>unk!';
    echo $string;
}

add_action('admin_notices', 'hgy_test');

//admin bar menü létrehozása
function toolbar_swapi_addon($wp_admin_bar)
{
    $args = array(
        'id' => 'hgy_swapi',
        'parent' => 'top-secondary',
        'title' => 'Hello admin bar'//icon
        /*,
        //hover doboz:
            'meta' => array(
                'html' => '<div>test</div>',
                'class' => 'hgy-swapi'
            )*/
    );
    $wp_admin_bar->add_node($args);
}

add_action('admin_bar_menu', 'toolbar_swapi_addon');

//admin bezárható ütenetek
function hgy_swapi_admin_notice__success()
{
    ?>
    <div class="notice notice-success is-dismissible">
        <h3>Ez az üzenet címe'</h3>
        <p><?php _e('Yikes!!!', 'sample-text-domain'); ?></p>
    </div>
    <?php
}

//add_action('admin_notices', 'hgy_swapi_admin_notice__success');

function hgy_swapi_admin_notice__warning()
{
    ?>
    <div class="notice notice-warning is-dismissible">
        <h3>Ez az üzenet címe'</h3>
        <p><?php _e('Oooops!!!', 'sample-text-domain'); ?></p>
    </div>
    <?php
}

//add_action('admin_notices', 'hgy_swapi_admin_notice__warning');

function hgy_swapi_admin_notice__error()
{
    ?>
    <div class="notice notice-error is-dismissible">
        <h3>Ez az üzenet címe'</h3>
        <p><?php _e('Run, Run away, we are lost!!!', 'sample-text-domain'); ?></p>
    </div>
    <?php
}

//add_action('admin_notices', 'hgy_swapi_admin_notice__error');

//írjunk a footerbe
function hgy_swapi_footer_addons()
{
    echo '<div style="text-align: center;background:rgba(0,0,0,0.1)">Made with ❤ and Wordpress</div>';
}

add_action('wp_footer', 'hgy_swapi_footer_addons');

//shortcode paraméterrel
function hgy_swapi_shortcode($atts = [], $content = null, $tag = '')
{
    // normalize attribute keys, lowercase
    $atts = array_change_key_case((array)$atts, CASE_LOWER);

    // override default attributes with user attributes
    $wporg_atts = shortcode_atts([
        'title' => 'Star Wars API teszt',
    ], $atts, $tag);

    // start output
    $o = '';

    // start box
    $o .= '<div class="hgy-swapi-box">';

    // title
    $o .= '<h2>' . esc_html__($atts['title'], 'hgy-swapi') . '</h2>';
// enclosing tags
    if (!is_null($content)) {
        // secure output by executing the_content filter hook on $content
        //$o .= apply_filters('the_content', $content);

        // run shortcode parser recursively
        $o .= do_shortcode($content);
    }

    // end box
    $o .= '</div>';

    // return output
    return $o;
}

function hgy_swapi_shortcodes_init()
{
    add_shortcode('hgy-swapi', 'hgy_swapi_shortcode');
}

add_action('init', 'hgy_swapi_shortcodes_init');
/**
 * @usage
 * [hgy-swapi title="Teszt cím"]
 * Lorem ipsum dolor sit amet, consectetur adipisicing elit. Assumenda, nisi reprehenderit. Ab aliquam, architecto aut doloribus mollitia necessitatibus nulla odit officia officiis quasi quo repudiandae tempora, totam voluptate voluptatum? Libero?
 * [/hgy-swapi]
 */
/**
 * @todo: a lista egy karakterre kattintva cserélje a dobozt a karakter lapjára ahol a back visszamegy a lista első elemére
 * @todo extra, a wp leírás alapján ez kerüljön ki egy js file ba amit betöltesz a scriptek közé (enqueue)
 */
function hgy_swapi_get_json()
{
    //$test = file_get_contents("https://swapi.co/api/people/5");
    echo
    '<script type="text/javascript">
 jQuery(document).ready( function($){
    //lapozó gombok működése
    $(document).on("click", ".hgy-swapi-box .prev", function($) {
            let targetUrl = jQuery(".hgy-swapi-box .prev").data("target")
            getPeople(targetUrl);
        });
    $(document).on("click", ".hgy-swapi-box .next", function() {
            let targetUrl = jQuery(".hgy-swapi-box .next").data("target");
            getPeople(targetUrl);
    });
   //egy karakter click
     $(document).on("click", ".hgy-swapi-box .charList li", function(e) {
            let url = this.getAttribute("data-target").replace(/\/$/, \'\');//data-targetből (utolsó / eltávolítva)
            let char = url.substring(url.lastIndexOf(\'/\') + 1);//csak a végén az azonosító
            getCharacter(char);//karakter betöltése
    });
    //karakterlapon vissza gomb
    $(document).on("click", ".hgy-swapi-box .back", function() {
            getPeople();
    });
        //összes karakter betöltése:     
        function getPeople(myUrl = null){
            let url ="https://swapi.co/api/people";
            if(myUrl !== null){
                url = myUrl
            }
            //console.log(url);
            $.ajax({
                type: "GET",
                datatype: "json",
                url: url, 
                success: function(result){
                  //console.log(result);
                  let el=$(".hgy-swapi-box");
                  let content = "<h2>Karakterek</h2>" +
                        "<ul class=\'charList\'>";
                         //eredmények bejárása
                   $.each(result.results, function( index, value ) {
                          content += "<li data-target=\'"+ value.url +"\' >" + value.name + "</li>" 
                        });
                          content  += "</ul>";
                          //előző oldal gomb
                          if(result.previous !== null ){
                              content  += "<button data-target=\'"+ result.previous +"\' class=\'btn prev\'>prev</button>"
                          }
                          ////következő oldal gomb
                          if(result.next !== null ){
                              content  += " <button data-target=\'"+ result.next +"\' class=\'btn next\'>next</button>"
                          }
                          el.html(content);
                  
                },
                error: function(request, status, error){
                    //console.log(request.responseText)
                }
            });
        }
        //teszt:      
        getPeople();
        //egy karakter betöltése
        function getCharacter(char){
            let url = "https://swapi.co/api/people/" + char;
             getAjax(url);
        }
        //teszt:
        //getCharacter(4);
        
        //ajax kérés ugyanaz, tehát betehetjük egy eljárásba azt is
        function getAjax(url){
            if( url ){
                $.ajax({
                    type: "GET",
                    datatype: "json",
                    url: url, 
                    success: function(result){
                      //console.log(result)
                      let el=$(".hgy-swapi-box");
                      if(el){
                          //ha megvan a doboz
                          let content = "<h2>" + result.name + "</h2>"
                          + "<ul>" +
                           "<li><b>Gender:</b> " + result.gender + "</li>" +
                           "<li><b>Eye Color:</b> " + result.eye_color + "</li>" +
                           "<li><b>Height:</b> " + result.height + "</li>" +
                           "<li><b>Weight:</b> " + result.mass + "</li>" +
                            "</ul>"
                            + "<button class=\'btn back\'>back</button>";
                          
                          el.html(content);
                      }
                    },
                    error: function(request, status, error){
                        //console.log(request.responseText)
                    }
                });
            }
        }
     });
</script>';
}

add_action('wp_footer', 'hgy_swapi_get_json');
